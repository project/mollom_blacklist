Mollom does a terrific job in catching and preventing spam, although recently 
some of my sites have still been getting spammed as Mollom isn't catching 
everything. I have been manually processing uncaught spam and adding the links 
to my mollom blacklist. This modules allows admins to flag content from 
content list and add it to a blacklist queue. The queue can be used to 
choose which links should be added to the sites mollom blacklist.
