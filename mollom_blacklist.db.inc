<?php 

/**
 * @file
 * Database functions for mollom_blacklist
 */

/**
 * Retrieve url scheme.
 *
 * @param string $scheme
 *   Url scheme
 *
 * @return object
 *   Object containing scheme id
 */
function mollom_blacklist_get_scheme($scheme) {
  return db_fetch_object(db_query("SELECT sid FROM {mollom_blacklist_url_scheme} WHERE scheme LIKE '%s'", $scheme));
}

/**
 * Retrieve url host.
 *
 * @param string $sid
 *   Scheme id
 * @param string $host
 *   Host
 *
 * @return object
 *   Object containing host id
 */
function mollom_blacklist_get_host($sid, $host) {
  return db_fetch_object(db_query("SELECT hid FROM {mollom_blacklist_url_host} WHERE host LIKE '%s' AND sid=%d", $host, $sid));
}

/**
 * Retrieve url path.
 *
 * @param string $path
 *   Path
 * @param string $hid
 *   Host id
 *
 * @return object
 *   Object containing path id
 */
function mollom_blacklist_get_path($path, $hid) {
  return db_fetch_object(db_query("SELECT pid FROM {mollom_blacklist_url_path} WHERE path LIKE '%s' AND hid=%d", $path, $hid));
}

/**
 * Retrieve urls for a user.
 *
 * @param int $uid
 *   User id
 *
 * @return array
 *   Array of urls
 */
function mollom_blacklist_get_urls($uid) {
  $urls = array();
  $result = db_query("
    SELECT COUNT(*) as cnt, uuid, path, host, scheme FROM {mollom_blacklist_url_user} uu
	JOIN {mollom_blacklist_url_path} up ON uu.pid = up.pid
	JOIN {mollom_blacklist_url_host} uh ON uh.hid = up.hid
	JOIN {mollom_blacklist_url_scheme} us ON us.sid = uh.sid
	WHERE uid = %d
	GROUP BY host, path",
    $uid
  );
  while ($row = db_fetch_object($result)) {
    $urls[$row->uuid] = $row;
  }
  return $urls;
}

/**
 * Retrieve a url.
 *
 * @param int $url_id
 *   Url id
 *
 * @return object
 *   Object containing url info
 */
function mollom_blacklist_get_url($url_id) {
  return db_fetch_object(db_query("
  SELECT * FROM {mollom_blacklist_url_user} uu
  JOIN {mollom_blacklist_url_path} up ON uu.pid = up.pid
  JOIN {mollom_blacklist_url_host} uh ON uh.hid = up.hid
  JOIN {mollom_blacklist_url_scheme} us ON us.sid = uh.sid
  WHERE uuid=%d",
  $url_id));
}
