<?php

/**
 * @file
 * Admin functionality for mollom_blacklist
 */

require_once 'mollom_blacklist.db.inc';

/**
 * Form submission handler for mollom_blacklist_form_node_admin_content.
 */
function mollom_blacklist_node_admin_overview_submit($form, &$form_state) {
  if (strpos($form_state['values']['operation'], 'mollomblacklist-') === 0) {
    $operations = explode('-', $form_state['values']['operation']);
    $id = array_shift($operations);
    foreach ($form_state['values']['nodes'] as $nid => $value) {
      if ($value) {
        if ($node = node_load($nid)) {
          foreach ($operations as $operation) {
            if ($operation == 'blacklist') {
              preg_match_all("/a[\s]+[^>]*?href[\s]?=[\s\"\']+" . "(.*?)[\"\']+.*?>" . "([^<]+|.*?)?<\/a>/", $node->body, $matches);
              foreach ($matches[1] as $url) {
                $parsed_url = parse_url($url);
                $scheme = new stdClass();
                $scheme->sid = mollom_blacklist_get_scheme($parsed_url['scheme'])->sid;
                if (!$scheme->sid) {
                  $scheme->scheme = $parsed_url['scheme'];
                  drupal_write_record('mollom_blacklist_url_scheme', $scheme);
                }
                $host = new stdClass();
                $host->hid = mollom_blacklist_get_host($scheme->sid, $parsed_url['host'])->hid;
                if (!$host->hid) {
                  $host->sid = $scheme->sid;
                  $host->host = $parsed_url['host'];
                  drupal_write_record('mollom_blacklist_url_host', $host);
                }
                if ($host->hid) {
                  $path = new stdClass();
                  $path->pid = mollom_blacklist_get_path($parsed_url['path'], $host->hid)->pid;
                  if (!$path->pid) {
                    $path->hid = $host->hid;
                    $path->path = $parsed_url['path'];
                    drupal_write_record('mollom_blacklist_url_path', $path);
                  }
                }
                if ($path->pid) {
                  global $user;
                  $url_user = new stdClass();
                  $url_user->pid = $path->pid;
                  $url_user->uid = $user->uid;
                  $url_user->date_created = time();
                  drupal_write_record('mollom_blacklist_url_user', $url_user);
                }
              }
            }
          }
        }
      }
    }

    cache_clear_all();

    if ($operation == 'delete') {
      drupal_set_message(t('The selected posts have been reported as inappropriate and are deleted.'));
    }
    elseif ($operation == 'unpublish') {
      drupal_set_message(t('The selected posts have been reported as inappropriate and are unpublished.'));
    }
    elseif ($operation == 'blacklist') {
      drupal_set_message(t('The selected posts have been analyzed and all urls in the post have been added to the mollom blacklist'));
    }
  }
}

/**
 * Generates the blacklist form.
 *
 * @param array $form_state
 *   Form state variable
 */
function mollom_blacklist_admin_blacklist_form(&$form_state) {
  global $user;
  $blacklisted = array();
  $urls = mollom_blacklist_get_urls($user->uid);
  $path = drupal_get_path('module', 'mollom') . '/mollom.info';
  $info = drupal_parse_info_file($path);
  // This datestamp determines which version of Mollom you are using.
  // There are two ways to get the blacklist depending on Mollom version.
  if ($info['datestamp'] >= 1335344178) {
    $blacklist = mollom()->getBlacklist();
    $index = 'value';
  }
  else {
    $blacklist = mollom('mollom.listBlacklistText');
    $index = 'text';
  }
  foreach ($blacklist as $url) {
    if ($url['context'] !== 'links') {
      continue;
    }
    $blacklisted[$url[$index]] = $url['match'];
  }
  $form['blacklisted'] = array(
    '#type' => 'hidden',
    '#value' => $blacklisted,
  );
  $form['urls'] = array(
    '#type' => 'checkboxes',
    '#options' => $urls,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Blacklist'),
  );
  $form['#submit'][] = 'mollom_blacklist_admin_blacklist_form_submit';
  $form['#theme'] = "admin_blacklist_form";
  return $form;
}

/**
 * Themes the blacklist form.
 *
 * @param array $form
 *   Form variable
 */
function theme_admin_blacklist_form($form) {
  $blacklisted = $form['blacklisted']['#value'];
  $headers = array('', t('Url'), t('Frequency'), t('Blacklisted'));
  foreach ($form['urls']['#options'] as $i => $url) {
    unset($form['urls'][$i]['#title']);
    $domain_str = $url->scheme . '://' . $url->host;
    $rows[] = array(
      drupal_render($form['urls'][$i]),
      $domain_str . $url->path,
      $url->cnt,
      isset($blacklisted[$domain_str . $url->path]) ? theme('image', 'misc/watchdog-ok.png') : "",
    );
  }
  $output = theme('table', $headers, $rows);
  $output .= drupal_render($form);
  return $output;
}

/**
 * Submit handler for the blacklist form.
 *
 * @param array $form
 *   Form variable
 * @param array $form_state
 *   Form state variable
 */
function mollom_blacklist_admin_blacklist_form_submit($form, $form_state) {
  $path = drupal_get_path('module', 'mollom') . '/mollom.info';
  $info = drupal_parse_info_file($path);
  foreach ($form_state['values']['urls'] as $url_id => $checked) {
    if ($checked) {
      $url = mollom_blacklist_get_url($url_id);
      $data = array();
      $url_str = $url->scheme . '://' . $url->host . $url->path;
      $data['context'] = 'links';
      $data['match'] = 'contains';
      $data['reason'] = 'spam';
      if ($info['datestamp'] >= 1335344178) {
        $data['value'] = $url_str;
        mollom()->saveBlacklistEntry($data);
      }
      else {
        $data['text'] = $url_str;
        $result = mollom('mollom.addBlacklistText', $data);
      }
    }
  }
}
